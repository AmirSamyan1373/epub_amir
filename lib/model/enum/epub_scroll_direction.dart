part of 'package:epub_amir/epub_amir.dart';

/// enum from scrollDirection to make it easier for users
enum EpubScrollDirection { HORIZONTAL, VERTICAL, ALLDIRECTIONS }
